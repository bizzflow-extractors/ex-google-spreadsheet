FROM python:3.10-bullseye

WORKDIR /code

ADD . .

RUN pip install --no-dependencies --no-cache-dir -r requirements.txt

# Edit this any way you like it

ENTRYPOINT ["python", "-u", "src/main.py"]
