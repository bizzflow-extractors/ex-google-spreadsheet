<!-- TODO: Huge refactor candidate, code repetition, unnecessary `pandas` usage, etc. -->

# Google Sheet Extractor

- **author:** Patrik Samko (patrik.samko@bizztreat.com)
- **created:** 08.08.2019
- **updated:** 17.03.2021
- **updated:** 15.11.2023 - bump python version and update dockerfile, added retry on APIError eceptions

---

## Description

Dockerized Bizzflow extractor for Google Spreadsheets.

### What it is used for

We use it for extracting Google Spreadsheets data.

### Why did you create it

Because is usefull for many projects we do.

### What the code does

The scripts does:

1. main.py creates a worker
2. worker get the service client and create an connection
3. worker extract the data
4. worker save the data

---

## Requirements

Requirements for running this project are (`points 1-4 do only if you don not use dockerfile`):

1. python 3.5 or higher
2. install pandas & numpy libraries
3. install [gspread](https://gspread.readthedocs.io/en/latest/) library
4. install [oauth2client](https://gspread.readthedocs.io/en/latest/oauth2.html) library (depends on your system also PyOpenSSL)
5. create service account json key and set privileges for this key to specific file or folder in your Google Drive - in the section `Create Google Service Account`
6. create the config file (example below)

Example of the config.json file:

```json
{
  "spreadsheets": {
    "https://docs.google.com/spreadsheets/d/1WSEeE4w9lfvbqyN7g5-4K3e9zpCeBEacnZ_bXuyNl9Y/edit#gid=1104857810": [
      1, 2
    ],
    "id_list,food_name": [1],
    "1SU32xmXCGPWQ7UUT1-v45okwmAaENOnl27Qjs4yCyY0": [1]
  },
  "service_account": {
    "type": "service_account",
    "project_id": "quickstart-156560125",
    "private_key_id": "215085d64d1cc4902f1d959f85",
    "private_key": "<your private key>",
    "client_email": "google-drive-service-account@quickstart-1565179460125.iam.gserviceaccount.com",
    "client_id": "114226810771122",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/google-drive-service-account%40quickstart-1560125.iam.gserviceaccount.com"
  },
  "scope": [
    "https://spreadsheets.google.com/feeds",
    "https://www.googleapis.com/auth/drive"
  ],
  "output_folder": "/data/out/tables/",
  "debug": true
}
```

### Create Google Service Account

a) If you have Bizzflow project, which is not in Google Cloud Platform (GCP):

- you have to create a GCP project and then continue with the steps below.

b) If your Bizzflow project is installed in GCP, just continue with the steps below.

#### How to create a Google Service Account?

1. Go to the [Google developer’s console](https://console.developers.google.com/) and select or create a project.

2. Now, we need to enable the APIs that we require in this project. Click on the Enable APIs and Services button to search for the APIs that Google provides.

   <img src="./doc/img/enable-api.png" style="width:800px;"/>

   Consequently, we will add two APIs for our project.

   - Google Sheets API
   - Google Drive API

3. In the search bar, search for these APIs and click on the enable button.

   <img src="./doc/img/welcome_to_the_api_library.png" style="width:800px;"/>

   - Google Sheets API will look something like this. It will allow you to access Google Spreadsheets. You would be able to read and modify the content present in the Spreadsheets.

       <img src="./doc/img/google_sheets_api_enable.png" style="width:800px;"/>

   - Google Drive API will look something like this. It will allow you to access the resources from Google Drive.

       <img src="./doc/img/google_drive_api_enable.png" style="width:800px;"/>

4. Once you have enabled the required APIs in your project then it’s time to create credentials for the service account. In the right menu click on the IAM & Admin -> Service Accounts button to continue.

<img src="./doc/img/click_on_service_account.png" style="width:800px;"/>

5. Click on create `CREATE SERVICE ACCOUNT`.

<img src="./doc/img/create_service_account_button.png" style="width:800px;"/>

6. Name your service account and optionally write a description. Then click on blue `DONE` button.

<img src="./doc/img/name_service_account.png" style="width:800px;"/>

7. Now you should see your service account in the list of all service accounts. Click on the three dots next to the service account and click in the pop-up menu on `Manage Keys`.

<img src="./doc/img/manage_keys.png" style="width:800px;"/>

8. Click on menu button `ADD KEY` and then on button `Create new key`.

<img src="./doc/img/create_new_key.png" style="width:800px;"/>

9. Leave checked `JSON` and hit the button `CREATE`.

<img src="./doc/img/json_key.png" style="width:800px;"/>

10. Now, a JSON file will be downloaded which contains the keys to access the API. Our google service account is ready to use.

    <img src="./doc/img/saved_json.png" style="width:800px;"/>

---

## How to use it

### Bizzflow use

1. [Setup the extractor & config file in Bizzflow](https://wiki.bizzflow.net/#/user-guide?id=extractors)
2. Run the extractor in Bizzflow.

### Local use

1. In the root folder create folder config and add there your config.json.
2. Run main.py script

## TODO

### Things which are not completely done

- none

### Things which could improve the current project/program/script

- later on we could create also GS writer ;)

## OTHER

- 8-08-2019 - tested on 3 data files via name, key and URL of the file
