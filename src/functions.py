
# -*- coding: utf-8 -*-
# pylint: enable=C,R,W
# pylint: disable=unused-variable

"""
functions.py

Created: 08.08.2019
Author: Patrik Samko (patrik.samko@bizztreat.com)

Usefull functions, which are used in other scripts.

Requirements: os, json
Config file: config file is described in README.md
Usage: you can use function defined in this script
"""
# Imports
import os
import json
import csv
import logging

# Global variables & Constants
CONFIG_LOCATION = "/config/config.json"

# Function definitions

logger = logging.getLogger(__name__)

def get_config():
    """
    Load configuration from json file.
    """
    if not os.path.exists(CONFIG_LOCATION):
        raise Exception("Configuration not specified")

    with open(CONFIG_LOCATION) as conf_file:
        conf = json.loads(conf_file.read())

    return conf


def create_directory(dir_path):
    """
    Create target Directory if don't exist
        :param dir_path: directory path
    """
    if not os.path.exists(dir_path):
        os.makedirs(dir_path, exist_ok=True)
        logger.debug("Directory %s created.", dir_path)


def save_dataframe_as_csv(dataframe, path, file_name, sort_columns = False):
    """
    Save dataframe as a csv to specified directory.
        :param dataframe: datagrame to save
        :param path: output directory path
        :param file_name: output csv file name
    """
    create_directory(path)
    translation_table = dict(zip(
        '''ěščřžýáíéťůúňóď/*+ -,.;´=&()%§$#[]<>:!^"'\n\r''',
        '''escrzyaietuunod____________________________'''
    ))

    # remove diacritics and other characters from the columns names
    df_col_names = {}
    for col_id, col_name in enumerate(dataframe.columns):
        if not col_name or not col_name.strip():
            logger.warning("Column no. %d has no header, it will be skipped", col_id + 1)
            dataframe = dataframe.drop(columns=[col_name])
            continue
        new_col_name = col_name.lower().translate(str.maketrans(translation_table))
        if new_col_name[0].isdigit():
            new_col_name = "_{}".format(new_col_name)
        df_col_names.update({col_name : new_col_name})
    df = dataframe.rename(columns=df_col_names)
    if sort_columns:
        logger.info("Reordering the columns alphabeticaly")
        df = df.reindex(sorted(df.columns), axis=1)
    file_name = file_name.lower().translate(str.maketrans(translation_table))
    df.to_csv(os.path.join(path, file_name + ".csv"),
              sep=',', index=False, encoding='utf-8', quoting=csv.QUOTE_ALL)
    logger.info("File '{}{}{}' saved.".format(path, file_name, ".csv"))
