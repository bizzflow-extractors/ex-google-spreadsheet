# -*- coding: utf-8 -*-
# pylint: enable=C,R,W
# pylint: disable=unused-variable

"""
worker.py

Created: 08.08.2019
Author: Patrik Samko (patrik.samko@bizztreat.com)

Worker use the client from service to get(extract) the data
from specified spreadsheet (in config.json) and save them
to specified disk space (again in config.json).

Requirements: python3.5 and higher, pandas
Config file: config file is described in README.md
Usage: create an instance of Worker and run methods get_data() and save_data()
"""
### Imports
import logging
import pandas as pd
from requests import Timeout

from service import Service
from gspread.exceptions import APIError
from typing import Optional
import backoff

from functions import save_dataframe_as_csv


class RetryableError(Exception):
    """Raised when an error we should try to overcome occurrs"""

    def __init__(self, detail: Optional[str] = None):
        super().__init__(detail or "An unexpected error occurred but we will try again")


logger = logging.getLogger(__name__)


class Worker:
    """
    Worker get and save the data using the service client.
    """

    def __init__(self, service=Service()):
        """
        Class contructor - get the client, spreadsheets ids other
        information from Service, which contains config.
            :param service: service which can give us a GS client
                we can use different Services with different
                configs for each Worker.
        """
        self.client = service.get_client()
        self.spreadsheets = service.conf["spreadsheets"]
        self.output_folder = service.conf["output_folder"]
        self.sort_columns = service.conf.get("sort_columns", False)
        self.data = []

    def get_data(self):
        """
        Get(extract) the data from spreadsheets.
        """
        logger.info("Getting the data.")
        for spreadsheet_id, worksheets_ids in self.spreadsheets.items():
            self.get_spreadsheet_data(spreadsheet_id, worksheets_ids)


    @backoff.on_exception(
        backoff.expo,
        exception=(RetryableError, Timeout),
        max_tries=10,
    )
    def get_spreadsheet_data(self, spreadsheet_id, worksheets_ids):
        logger.info("Getting the data from spreadsheet: %s", spreadsheet_id)
        # if the spreadsheet id is an url
        try:
            if "https://" in spreadsheet_id:
                spreadsheet = self.client.open_by_url(spreadsheet_id)
            else:
                spreadsheet = self.client.open_by_key(spreadsheet_id)
            s_name = spreadsheet.fetch_sheet_metadata()["properties"]["title"]
            for worksheet_id in worksheets_ids:
                logger.info("Getting the data from worksheet: %s", worksheet_id)
                w_id = int(worksheet_id) - 1
                w_data = spreadsheet.get_worksheet(w_id).get_all_records()
                d_name = "{}-{}".format(s_name, worksheet_id)
                data_record = {d_name: w_data}
                self.data.append(data_record)
        except APIError as error:
            if error.response.status_code in (408, 429, 500, 502, 503, 504):
                raise RetryableError("This worksheet specified by URL:{} cannot be reached at this point in time, however it will be retried. \
                        \n Error: \
                        \n{}".format(spreadsheet_id, error)) from error
            raise error


    def save_data(self):
        logger.info("Saving the data.")
        for data_record in self.data:
            for name, data in data_record.items():
                df = pd.DataFrame(data)
                save_dataframe_as_csv(df, self.output_folder, name, sort_columns=self.sort_columns)
