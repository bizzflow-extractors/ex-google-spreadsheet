
# -*- coding: utf-8 -*-
# pylint: enable=C,R,W
# pylint: disable=unused-variable

"""
service.py

Created: 08.08.2019
Author: Patrik Samko (patrik.samko@bizztreat.com)

Create a Service which can give you and gspread client, which can operate with
Google Spreadsheets data.

Requirements: python3.5 and higher, oauth2client, gspread
Config file: config file is described in README.md
Usage: create an instance of class Service and call the method get_client()
"""
### Imports
import json
import logging
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from functions import get_config

# create connection based on project credentials

logger = logging.getLogger(__name__)
class Service():
    """
    Service could create a new GS client by calling get_client method.
    """
    def __init__(self, conf=get_config()):
        """
        Constructor of the class.
            :param conf:
        """
        self.conf = conf

    def get_client(self):
        """
        Get the GS client.
            :param returm: return the client
        """
        logger.info("Creating the service client.")
        if isinstance(self.conf["service_account"],str):
            self.conf["service_account"] = json.loads(self.conf["service_account"])
        try:
            creds = ServiceAccountCredentials.from_json_keyfile_dict(self.conf["service_account"],
                                                                     self.conf["scope"])
            client = gspread.authorize(creds)
            return client

        except Exception as e:
            raise Exception("Something wrong with your service account:\n{}".format(e))



