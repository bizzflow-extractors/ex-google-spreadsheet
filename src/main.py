
# -*- coding: utf-8 -*-
# pylint: enable=C,R,W
# pylint: disable=unused-variable

"""
main.py

Created: 08.08.2019
Author: Patrik Samko (patrik.samko@bizztreat.com)

Run the Google Spreadsheet Extractor.

Requirements: python-version, python libraries/modules
Config file: config file is described in README.md
Usage: run the main.py
"""

### Imports
import logging
import worker as w

### Function definitions

logging.basicConfig(level=logging.INFO)

def main():
    """
    Main function to run the Google Spreadsheet Extractor.
    """
    worker = w.Worker()
    worker.get_data()
    worker.save_data()

if __name__ == "__main__":
    main()
